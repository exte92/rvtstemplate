import { useState } from "react";
import { useTranslation } from "react-i18next";
import reactLogo from "@shared/assets/react.svg";
import React from "react";
import ReactDOM from "react-dom/client";

import "@shared/i18n";
import viteLogo from "/vite.svg";

import { ELanguagesVariants } from "@shared/model";
import { useLanguageSwitcher } from "@shared/hooks";

import "normalize.css";

export const App = () => {
  const { t } = useTranslation();
  const [count, setCount] = useState<number>(0);
  const { switchLanguage } = useLanguageSwitcher();

  return (
    <>
      <div>
        <a href="https://vitejs.dev" target="_blank" rel="noreferrer">
          <img src={viteLogo} className="logo" alt="Vite logo" />
        </a>
        <a href="https://react.dev" target="_blank" rel="noreferrer">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          {t("count")} {count}
        </button>
        <p>
          Edit <code>src/App.tsx</code> and save to test HMR
        </p>
      </div>
      <div>
        <button onClick={() => switchLanguage(ELanguagesVariants.RU)}>
          {t("ru")}
        </button>
        <button onClick={() => switchLanguage(ELanguagesVariants.EN)}>
          {t("en")}
        </button>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </>
  );
};

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
);
