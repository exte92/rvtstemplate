import { defineConfig } from "vite";
import svgr from "vite-plugin-svgr";
import react from "@vitejs/plugin-react";
import tsconfigPaths from "vite-tsconfig-paths";

// https://vitejs.dev/config/
export default defineConfig({
  envPrefix: ["VITE_", "V_"],
  plugins: [react(), svgr({ svgrOptions: { icon: true } }), tsconfigPaths()],
  server: {
    port: 80,
    host: true,
    strictPort: true,
    watch: {
      usePolling: true,
    },
    proxy: {
      // Прокси для запросов к /api
      "/api": {
        target: "https://dev.hot-peach.su/",
        changeOrigin: true, // Добавлено для работы с CORS
        // secure: false, // Отключаем проверку SSL, если целевой сервер использует самоподписанный сертификат
        // rewrite: (path) => path.replace(/^\/forum\/api/, "/api"), // Убираем префикс /api при пересылке запроса
      },
    },
  },
  preview: {
    port: 80,
    host: true,
    strictPort: true,
  },
  css: {
    preprocessorOptions: {
      scss: {
        api: "modern-compiler",
      },
    },
  },
});
